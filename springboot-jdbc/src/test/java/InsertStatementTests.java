import com.mytestjdbcapp.core.dao.CustomerDao;
import com.mytestjdbcapp.core.dao.Impl.CustomerDaoImpl;
import com.mytestjdbcapp.core.model.Customer;
import com.mytestjdbcapp.core.service.DriverManagerConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DriverManagerConfiguration.class})
@EnableConfigurationProperties
public class InsertStatementTests {
    public static final int cnt = 1000;
    @Autowired
    private DataSource dataSource;

    private CustomerDao getCustomerDao(){
        return new CustomerDaoImpl(dataSource);
    }

    @Before
    public void clearDB(){
        getCustomerDao().delete();
    }

    @Test
    public void insertStatementTest(){
        CustomerDao customerDao = getCustomerDao();
        long start, finish;
        ArrayList<Customer> customers = new ArrayList<>();
        for(int i = 0; i < cnt; i++){
            customers.add(new Customer(i, "username #" + i, new Date()));
        }
        start = System.currentTimeMillis();
        for (Customer customer : customers) {
            customerDao.addStmt(customer);
        }
        finish = System.currentTimeMillis();
        System.out.println("Statement insert time: " + (finish - start) + " millis.");
        //customerDao.get().forEach(System.out::println);
        Assert.assertEquals(customerDao.get().size(), cnt);

    }

    @Test
    public void insertPrepareStatementTest(){
        CustomerDao customerDao = getCustomerDao();
        long start, finish;
        ArrayList<Customer> customers = new ArrayList<>();
        for(int i = 0; i < cnt; i++){
            customers.add(new Customer(i, "username #" + i, new Date()));
        }

        start = System.currentTimeMillis();
        for (Customer customer : customers) {
            customerDao.addPrepStmt(customer);
        }
        finish = System.currentTimeMillis();
        System.out.println("Prepare Statement insert time: " + (finish - start) + " millis.");
        //customerDao.get().forEach(System.out::println);
        Assert.assertEquals(customerDao.get().size(), cnt);
    }

    @Test
    public void insertBatchTest(){
        CustomerDao customerDao = getCustomerDao();
        long start, finish;
        ArrayList<Customer> customers = new ArrayList<>();
        for(int i = 0; i < cnt; i++){
            customers.add(new Customer(i, "username #" + i, new Date()));
        }

        start = System.currentTimeMillis();
        customerDao.addBatch(customers);
        finish = System.currentTimeMillis();
        System.out.println("Batch insert time: " + (finish - start) + " millis.");
        //customerDao.get().forEach(System.out::println);
        Assert.assertEquals(customerDao.get().size(), cnt);
    }
}
