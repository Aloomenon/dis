package com.mytestjdbcapp.core.model;

import java.util.Date;

public class Customer {

    private int id;

    private String name;

    private Date cdate;

    public Customer(int id, String name, Date cdate) {
        this.id = id;
        this.name = name;
        this.cdate = cdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    @Override
    public String toString(){
        return String.format("Customer:[id=%d, name='%s', cdate='%s']", id, name, cdate);
    }
}
