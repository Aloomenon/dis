package com.mytestjdbcapp.core.api;

import com.mytestjdbcapp.core.dao.CustomerDao;
import com.mytestjdbcapp.core.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(path="/customer")
public class CustomersController {

    private final CustomerDao customerDao;

    @Autowired
    public CustomersController(CustomerDao customerDao){
        this.customerDao = customerDao;
    }

    @GetMapping(path = "/add")
    public @ResponseBody
    String add(@RequestParam int id, @RequestParam String name){
        Customer customer = new Customer(id, name, new Date());
        customerDao.addPrepStmt(customer);
        return "Saved";
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    Customer getUserById(@PathVariable("id") int id){
        return customerDao.get(id);
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    List<Customer> getAllUsers(){
        return customerDao.get();
    }
}
