package com.mytestjdbcapp.core.dao.Impl;

import com.mytestjdbcapp.core.dao.CustomerDao;
import com.mytestjdbcapp.core.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    private final DataSource dataSource;

    @Autowired
    public CustomerDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addStmt(Customer customer) {
        String sql = "INSERT INTO test_data (ID, NAME, CDATE) VALUES (" +
                customer.getId() + ", " +
                "\'" + customer.getName() + "\', " +
                "\'" + new java.sql.Date(customer.getCdate().getTime()) + "\')";
        try (Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement()) {
            conn.setAutoCommit(false);
            stmt.executeUpdate(sql);
            conn.commit();
        } catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addPrepStmt(Customer customer) {
        String sql = "INSERT INTO test_data (ID, NAME, CDATE) VALUES (?, ?, ?)";

        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            ps.setInt(1, customer.getId());
            ps.setString(2, customer.getName());
            ps.setDate(3, new java.sql.Date(customer.getCdate().getTime()));
            ps.executeUpdate();
            conn.commit();
        } catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addBatch(List<Customer> customerList) {
        String sql = "INSERT INTO test_data (ID, NAME, CDATE) VALUES (?, ?, ?)";

        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)){
            conn.setAutoCommit(false);
            for(Customer customer : customerList){
                ps.setInt(1, customer.getId());
                ps.setString(2, customer.getName());
                ps.setDate(3, new Date(customer.getCdate().getTime()));
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();
        } catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Customer get(int id) {
        String sql = "SELECT * FROM test_data WHERE ID = ?";

        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Customer customer = null;
            if (rs.next()) {
                customer = new Customer(
                        rs.getInt("ID"),
                        rs.getString("NAME"),
                        rs.getDate("CDATE")
                );
            }
            rs.close();
            return customer;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Customer> get() {
        String sql = "SELECT * FROM test_data";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery()) {
            while(rs.next()){
                Customer customer = new Customer(rs.getInt("ID"),
                        rs.getString("NAME"),
                        rs.getDate("CDATE"));
                customers.add(customer);
            }
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        return customers;
    }

    @Override
    public void delete() {
        String sql = "TRUNCATE test_data";
        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

}
