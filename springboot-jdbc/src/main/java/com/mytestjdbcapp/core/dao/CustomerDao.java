package com.mytestjdbcapp.core.dao;

import com.mytestjdbcapp.core.model.Customer;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface CustomerDao {

    void addStmt(Customer customer);
    void addPrepStmt(Customer customer);
    void addBatch(List<Customer> customerList);
    Customer get(int id);
    List<Customer> get();
    void delete();
}
