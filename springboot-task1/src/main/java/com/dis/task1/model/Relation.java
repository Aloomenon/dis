package com.dis.task1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "RELATIONS")
public class Relation {

    @Id
    @Column(nullable = false)
    protected long id;
//    @Column(nullable = false)
//    protected Boolean visible;

    @Column(nullable = false, name = "username")
    protected String user;

    @Column(nullable = false, name = "date")
    protected Date timestamp;
//    @Column(nullable = false, name = "ver")
//    protected int version;
//
//    @Column(nullable = false)
//    protected long uid;
//
//    @Column(nullable = false)
//    protected long changeset;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public Boolean getVisible() {
//        return visible;
//    }
//
//    public void setVisible(Boolean visible) {
//        this.visible = visible;
//    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

//    public int getVersion() {
//        return version;
//    }
//
//    public void setVersion(int version) {
//        this.version = version;
//    }
//
//    public long getUid() {
//        return uid;
//    }
//
//    public void setUid(long uid) {
//        this.uid = uid;
//    }
//
//    public long getChangeset() {
//        return changeset;
//    }
//
//    public void setChangeset(long changeset) {
//        this.changeset = changeset;
//    }
}
