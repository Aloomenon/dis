package com.dis.task1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@Column(nullable = false)
    private int id;
    //@Column(nullable = false)
    private String key;
    //@Column(nullable = false)
    private String value;
    @ManyToOne
    @JoinColumn(name = "parent_node")
    @JsonIgnore
    private Nodes parentNode;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Nodes getParentNode() {
        return parentNode;
    }

    public void setParentNode(Nodes parentNode) {
        this.parentNode = parentNode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
