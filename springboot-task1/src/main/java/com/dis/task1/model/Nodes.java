package com.dis.task1.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "Nodes")
@Table(name = "NODES")
public class Nodes {

    @Id
    //@Column(nullable = false)
    private long id;
    //@Column(nullable = false)
    private long uid;
    //@Column(nullable = false, name = "username")
    private String username;
    @Column(nullable = false, name = "ver")
    private int version;
    @Column(nullable = false, name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    //@Column(nullable = false)
    private long changeset;
    //@Column(nullable = false)
    private double lon;
    //@Column(nullable = false)
    private double lat;
    @Column(nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parentNode")
    private List<Tag> tag;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }


    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }


    public long getChangeset() {
        return changeset;
    }

    public void setChangeset(long changeset) {
        this.changeset = changeset;
    }


    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public List<Tag> getTag() {
        return tag;
    }


    public void setTag(List<Tag> tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "Nodes{" +
                "username='" + username + '\'' +
                ", id=" + id +
                ", uid=" + uid +
                ", version=" + version +
                ", timestamp=" + timestamp +
                ", changeset=" + changeset +
                ", lon=" + lon +
                ", lat=" + lat +
                ", tag=" + tag +
                '}';
    }


}
