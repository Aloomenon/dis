package com.dis.task1.service;

import com.dis.task1.dao.TagsDao;
import com.dis.task1.dao.nodes.NodesDao;
import com.dis.task1.dao.relations.RelationsDao;
import com.dis.task1.dao.ways.WayDao;
import com.dis.task1.repository.NodesRepository;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmNode;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmRelation;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmTag;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmWay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DBService {

    private final Logger logger = LoggerFactory.getLogger(DBService.class);
    private final NodesDao nodesDao;
    private final WayDao wayDao;
    private final RelationsDao relationsDao;
    private final TagsDao tagsDao;


    @Autowired
    public DBService(NodesDao nodesDao, RelationsDao relationsDao, WayDao wayDao, TagsDao tagsDao) {
        this.nodesDao = nodesDao;
        this.relationsDao = relationsDao;
        this.wayDao = wayDao;
        this.tagsDao = tagsDao;
    }

    public void importIndex(){
        long start = System.currentTimeMillis(), stop;
        nodesDao.setIndex();
    }

    public void importToDB(List<Object> list) {
        long start = System.currentTimeMillis(), stop;
        if (!list.isEmpty()) {
            Object item = list.get(0);
            if (item instanceof OsmWay) {
                wayDao.addBatch((List<OsmWay>) (Object) list);
                logger.info(list.size() + " Ways in batch. Insert time: " + (System.currentTimeMillis() - start) + " millis.");
            } else if (item instanceof OsmRelation) {
                relationsDao.addBatch((List<OsmRelation>) (Object) list);
                logger.info(list.size() + " Relations in batch. Insert time: " + (System.currentTimeMillis() - start) + " millis.");
            } else if (item instanceof OsmNode) {
                nodesDao.addBatch((List<OsmNode>) (Object) list);
                logger.info(list.size() + " Nodes in batch. Insert time: " + (System.currentTimeMillis() - start) + " millis.");
            }
        }
    }

    public void importToDB(Map<Long, List<OsmTag>> tags) {
        long start = System.currentTimeMillis(), stop;
        //logger.info("size: " + tags.size());
        if (tags == null) {
            return;
        }
        tagsDao.addBatch(tags);
        //stop = System.currentTimeMillis();
        //logger.info(tags.size() + " Tags in batch. Insert time: " + (stop - start) + " millis.");
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor(){
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(4);
        pool.setMaxPoolSize(16);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }

}
