package com.dis.task1.service;

import com.dis.task1.utils.xml.OrmParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XmlWorkerService {

    private final Logger logger = LoggerFactory.getLogger(XmlWorkerService.class);
    private final DBService dbService;
    private final OrmParser ormParser;

    @Autowired
    public XmlWorkerService(DBService dbService, OrmParser ormParser) {
        this.dbService = dbService;
        this.ormParser = ormParser;

    }

    public void fillNodes(String decompressedFile) {
        //JAXB jaxb = new JAXB( dbService, taskExecutor());
        ormParser.setXmlPath(decompressedFile);
        //jaxb.xmlUnmarshal();
    }
}
