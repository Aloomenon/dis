package com.dis.task1.rest;

import com.dis.task1.model.Nodes;
import com.dis.task1.repository.NodesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/nodes")
public class NodesController {
    private static final double KM_TO_ML = 0.62136119;
    private final NodesRepository nodesRepository;

    @Autowired
    public NodesController(NodesRepository nodesRepository) {
        this.nodesRepository = nodesRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<String> createNode(@RequestBody Nodes node) {
        if (nodesRepository.exists(node.getId())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        nodesRepository.save(node);
        return ResponseEntity.ok(node.toString());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Nodes> getNode(@PathVariable Long id) {
        Nodes node = nodesRepository.findOne(id);
        if (node == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(node);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Nodes> updateNode(@PathVariable Long id, @RequestBody Nodes newnode) {
        Nodes node = nodesRepository.findOne(id);
        if (node == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        //node.setTag(newnode.getTag() != null ? newnode.getTag() : node.getTag());
        node.setTimestamp(newnode.getTimestamp() != null ? newnode.getTimestamp() : node.getTimestamp());
        node.setVersion(newnode.getVersion() != 0 ? newnode.getVersion() : node.getVersion());
        node.setUid(newnode.getUid() != 0 ? newnode.getUid() : node.getUid());
        node.setLon(newnode.getLon());
        node.setLat(newnode.getLat());
        node.setChangeset(newnode.getChangeset() != 0 ? newnode.getChangeset() : node.getVersion());
        node.setUsername(newnode.getUsername() != null ? newnode.getUsername() : node.getUsername());
        nodesRepository.save(node);
        return ResponseEntity.ok(node);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteNode(@PathVariable Long id) {
        if (nodesRepository.findOne(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        nodesRepository.delete(id);
        return ResponseEntity.ok("Node with id = " + id + " has been deleted.");
    }

    @RequestMapping(value = "/map", method = RequestMethod.GET)
    public ResponseEntity<?> getMap() {
        return ResponseEntity.ok(nodesRepository.getNodesMap());
    }

    @RequestMapping(value = "/radius", method = RequestMethod.GET)
    public ResponseEntity<List<Nodes>> getNodesInRange(@RequestParam double lat, @RequestParam double lon, @RequestParam int range) {
        return ResponseEntity.ok(nodesRepository.getNodesInRange(lat, lon, range * KM_TO_ML));
    }
}
