package com.dis.task1.utils.cli;

import com.dis.task1.service.XmlWorkerService;
import com.dis.task1.utils.bzipDecompressor.Decompressor;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Properties;

@Component
public class Cli  {

    private static Logger logger = LoggerFactory.getLogger(Cli.class);
    private Options options;
    private String filepath, decompressedFile;
    private boolean console = false;
    private final XmlWorkerService xmlWorkerService;

    @Autowired
    public Cli(XmlWorkerService xmlWorkerService) {
        this.xmlWorkerService = xmlWorkerService;
    }

//    @PostConstruct
//    public void init() {
//        options = new Options();
//        options.addOption("n", "name", true, "filename after decompress");
//        options.addOption("f", "file", true, "input file");
//        options.addOption("c", "console", false, "output in console");
//    }
//
//    @Override
//    public void run(String... args) {
//        logger.info("Parse input args.");
//        CommandLineParser parser = new DefaultParser();
//        Properties properties = System.getProperties();
//        try {
//            CommandLine cmd = parser.parse(options, args);
//            if (cmd.hasOption('n')) {
//                decompressedFile = cmd.getOptionValue('n');
//                properties.setProperty("decFilepath", decompressedFile);
//                logger.info("parsing decompressed file path: " + decompressedFile);
//            }
//            if (cmd.hasOption('f')) {
//                filepath = cmd.getOptionValue('f');
//                properties.setProperty("filepath", filepath);
//                logger.info("parsing archive path: " + filepath);
//            }
//            if (cmd.hasOption('c')) {
//                logger.info("Console output.");
//                console = true;
//                properties.setProperty("console", "true");
//            }
//        } catch (ParseException ex) {
//            logger.error("Cannot parse parameters. " + ex.getMessage());
//        }
//
//        //analyzeOptions();
//    }
    @Bean
    private String analyzeOptions() {
        Properties properties = System.getProperties();

        String filepath = properties.getProperty("filepath");
        String[] split = filepath.split("/");
        String decompressedFile = properties.getProperty("decFilepath", "output/" + split[split.length - 1].replace(".bz2", ""));
        logger.info("Xml file name: " + decompressedFile);
        boolean console = Boolean.valueOf(properties.getProperty("console", "false"));
//        if ("".equals(decompressedFile) || null == decompressedFile) {
//            String[] split = filepath.split("/");
//            decompressedFile = "output/" + split[split.length - 1].replace(".bz2", "");
//        }

        if (!new File(decompressedFile).exists()) {
            Decompressor decompressor = new Decompressor();
            decompressor.decompress(filepath, decompressedFile, 500 * 1024 * 1024);
        }
        /*For 1st task*/
        //service.fillUsersMap(decompressedFile);

        /*For 2nd task*/
        xmlWorkerService.fillNodes(decompressedFile);
//        service.fillUsersMap();
//

        return decompressedFile;
    }
}
