package com.dis.task1.utils.xml;

import com.dis.task1.service.DBService;
import com.dis.task1.utils.xml.schema.generatedClasses.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

@Component
public class OrmParser {

    private String xmlPath;
    private static Logger logger = LoggerFactory.getLogger(OrmParser.class);
    private List<OsmNode> nodes;
    private List<OsmRelation> relations;
    private List<OsmWay> ways;
    private Map<Long, List<OsmTag>> tags = new HashMap<>();

    private final ThreadPoolTaskExecutor taskExecutor;
    private final DBService service;

    @Autowired
    public OrmParser(DBService service, ThreadPoolTaskExecutor taskExecutor) {
        //this.xmlPath = xmlPath;
        this.service = service;
        this.taskExecutor = taskExecutor;
    }

    public void xmlUnmarshal() {
        int batchSize = 250000;
        JAXBContext context;
        logger.info("Ready for unmarshall " + xmlPath);
        long cnt = 0;
        try {

            File xmlFile = new File(xmlPath);
            XMLInputFactory xif = XMLInputFactory.newFactory();
            XMLStreamReader xsr = xif.createXMLStreamReader(new FileReader(xmlFile));
            nodes = new ArrayList<>();
            relations = new ArrayList<>();
            ways = new ArrayList<>();

            context = JAXBContext.newInstance(OsmNode.class, OsmWay.class, OsmTag.class, OsmRelation.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            boolean checked = false;
            String chars;
            SimpleAsyncTaskExecutor pool = new SimpleAsyncTaskExecutor();
            while (xsr.hasNext()) {
                switch (xsr.next()) {
                    case XMLStreamConstants.START_ELEMENT:
                        switch (xsr.getLocalName()) {
                            case "node":
                                JAXBElement<OsmNode> nodeElement = unmarshaller.unmarshal(xsr, OsmNode.class);
                                nodes.add(nodeElement.getValue());
                                List<OsmTag> nodeTags = nodeElement.getValue().getTag();
                                if (nodeTags != null && !nodeTags.isEmpty()) {
                                    tags.put(nodeElement.getValue().getId(), nodeTags);
                                }
                                if (nodes.size() == batchSize) {
                                    List<Object> list = new ArrayList<>((List<Object>) (Object) nodes);
                                    Map<Long, List<OsmTag>> map = tags.isEmpty() ? null : new HashMap<>(tags);
                                    taskExecutor.execute(() -> {
                                        service.importToDB(list);
                                        service.importToDB(map);
                                        list.clear();
                                        map.clear();
                                    });
                                    logger.info("Active threads: " + taskExecutor.getActiveCount());
                                    nodes.clear();
                                    tags.clear();
                                }
                                break;
                            case "relation":
                                JAXBElement<OsmRelation> relation = unmarshaller.unmarshal(xsr, OsmRelation.class);
                                relations.add(relation.getValue());
                                if (relations.size() == batchSize) {
                                    execute(new ArrayList<>((List<Object>) (Object) relations));
                                    relations.clear();
                                }

                                break;
                            case "way":
                                JAXBElement<OsmWay> way = unmarshaller.unmarshal(xsr, OsmWay.class);
                                ways.add(way.getValue());
                                if (ways.size() == batchSize) {
                                    execute(new ArrayList<>((List<Object>) (Object) ways));
                                    ways.clear();
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        chars = xsr.getText().trim();
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        break;

                }
            }
            if (!nodes.isEmpty()) {
                service.importToDB((List<Object>) (Object) nodes);
                nodes.clear();
                service.importToDB(tags);
                tags.clear();
            }
            if (!relations.isEmpty()) {
                service.importToDB((List<Object>) (Object) relations);
                relations.clear();
            }
            if (!ways.isEmpty()) {
                service.importToDB((List<Object>) (Object) ways);
                ways.clear();
            }
            long start = System.currentTimeMillis();
            logger.info("Start indexing NODES");
            service.importIndex();
            logger.info("Indexing finished. Time: " + (start - System.currentTimeMillis()));
        } catch (OutOfMemoryError ex) {
            logger.error("Out of memory... Size:" + cnt + " " + ex.getMessage());
        } catch (JAXBException ex) {
            logger.error("Cannot unmarshal xml:" + xmlPath + ". " + ex);
        } catch (XMLStreamException ex) {
            logger.error(ex.getMessage());
        } catch (FileNotFoundException ex) {
            logger.error(ex.getMessage());
        }
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    private void execute(List<Object> list) {
        taskExecutor.execute(() -> {
            service.importToDB(list);
            list.clear();
        });
    }
}
