package com.dis.task1.utils.bzipDecompressor;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class Decompressor {

    private static final Logger logger = LoggerFactory.getLogger(Decompressor.class);

    public void decompress(String path, String filename, int bufferSize) {
        logger.info("Decompressing file " + path);
        try (FileOutputStream out = new FileOutputStream(new File(filename));
             BZip2CompressorInputStream in = new BZip2CompressorInputStream(new FileInputStream(path))) {

            final byte[] buffer = new byte[bufferSize];
            int n;
            while (-1 != (n = in.read(buffer))) {
                out.write(buffer, 0, n);
            }
            logger.info("File decompressed into " + filename);
        } catch (IOException ex) {
            logger.error("Cannot decompress file: " + ex.getMessage());
        }
    }
}
