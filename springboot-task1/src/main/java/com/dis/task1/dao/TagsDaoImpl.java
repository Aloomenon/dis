package com.dis.task1.dao;

import com.dis.task1.utils.xml.schema.generatedClasses.OsmTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

@Repository
public class TagsDaoImpl implements TagsDao {

    private final DataSource dataSource;
    private final Logger logger = LoggerFactory.getLogger(TagsDaoImpl.class);

    @Autowired
    public TagsDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addBatch(Map<Long, List<OsmTag>> tags) {
        String sql = "INSERT INTO TAGS (KEY, VALUE, PARENT_NODE) VALUES (?, ?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            conn.setAutoCommit(false);
            tags.forEach((key, value) -> {
                try {
                    ps.setLong(3, key);
                    for (OsmTag tag : value) {
                        ps.setString(1, tag.getK());
                        ps.setString(2, tag.getV());
                    }
                    ps.addBatch();
                } catch (SQLException ex) {
                  logger.error("cannot map tag: " + ex.getMessage());
                }
                //logger.info(id + ":" + tag.getK() + "=>" + tag.getV());
            });
            ps.executeBatch();
            conn.commit();
        } catch (SQLException ex) {
            logger.error("CANNOT INSERT TAG: " + ex.getMessage());
        }
    }
}
