package com.dis.task1.dao.relations;

import com.dis.task1.utils.xml.schema.generatedClasses.OsmRelation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RelationsDao {
    public void addBatch(List<OsmRelation> relationList);
}
