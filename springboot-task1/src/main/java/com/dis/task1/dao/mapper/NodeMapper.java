package com.dis.task1.dao.mapper;

import com.dis.task1.utils.xml.schema.generatedClasses.OsmNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;


public class NodeMapper implements RowMapper{

    private Logger logger = LoggerFactory.getLogger(NodeMapper.class);

    public void map(PreparedStatement ps, OsmNode node) throws SQLException {
        ps.setLong(1, node.getId());
        ps.setString(2, node.getUser());
        ps.setInt(3, node.getVersion());
        ps.setLong(4, node.getUid());
        ps.setDate(5, new java.sql.Date(node.getTimestamp().toGregorianCalendar().getTime().getTime()));
        ps.setLong(6, node.getChangeset());
        ps.setDouble(7, node.getLon());
        ps.setDouble(8, node.getLat());
    }

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        OsmNode node = new OsmNode();
        node.setId(resultSet.getLong("ID"));
        node.setUser(resultSet.getString("USERNAME"));
        node.setChangeset(resultSet.getLong("CHANGESET"));
        node.setVersion(resultSet.getInt("VER"));
        node.setUid(resultSet.getLong("UID"));
        node.setLat(resultSet.getDouble("LAT"));
        node.setLon(resultSet.getDouble("LON"));
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(resultSet.getDate("DATE"));
        try {
            node.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
        } catch (DatatypeConfigurationException ex){
            logger.error("Cannot set DATE: " + ex.getMessage());
        }
        return node;
    }
}
