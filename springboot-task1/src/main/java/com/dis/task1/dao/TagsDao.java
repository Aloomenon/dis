package com.dis.task1.dao;

import com.dis.task1.utils.xml.schema.generatedClasses.OsmTag;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public interface TagsDao {
    void addBatch(Map<Long, List<OsmTag>> tags);
}
