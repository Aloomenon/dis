package com.dis.task1.dao.relations;

import com.dis.task1.dao.mapper.WayMapper;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmRelation;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmTag;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmWay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class RelationsDaoImpl implements RelationsDao{

    private final DataSource dataSource;
    private WayMapper mapper = new WayMapper();

    @Autowired
    public RelationsDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addBatch(List<OsmRelation> relationList) {
        String sql = "INSERT INTO RELATIONS (ID, USERNAME, DATE) " +
                "VALUES (?, ?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            conn.setAutoCommit(false);
            for (OsmRelation relation : relationList) {
                mapper.map(ps, relation);
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
