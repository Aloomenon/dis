package com.dis.task1.dao.nodes;

import com.dis.task1.dao.mapper.NodeMapper;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Repository
public class NodesDaoImpl implements NodesDao {

    private final DataSource dataSource;
    private static final Logger logger = LoggerFactory.getLogger(NodesDaoImpl.class);
    private NodeMapper mapper = new NodeMapper();

    @Autowired
    public NodesDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addStmt(OsmNode node) {
        String sep = ", ";
        String sql = "INSERT INTO NODES (ID, USERNAME, VER, UID, DATE, CHANGESET, LON, LAT) VALUES (" +
                node.getId() + sep +
                "\'" + node.getUser() + "\'" + sep +
                node.getVersion() + sep +
                node.getUid() + sep +
                "\'" + node.getTimestamp().toGregorianCalendar().getTime() + "\', " +
                node.getChangeset() + sep +
                node.getLon() + sep + node.getLat() + ")";
        logger.debug(sql);
        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {
            conn.setAutoCommit(false);
            stmt.executeUpdate(sql);
            conn.commit();
        } catch (SQLException e) {
            logger.error("Cannot execute sql. " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addPrepStmt(OsmNode node) {
        String sql = "INSERT INTO NODES (ID, USERNAME, VER, UID, DATE, CHANGESET, LON, LAT) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            conn.setAutoCommit(false);
            mapper.map(ps, node);
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addBatch(List<OsmNode> nodeList) {
        String sql = "INSERT INTO NODES (ID, USERNAME, VER, UID, DATE, CHANGESET, LON, LAT) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            conn.setAutoCommit(false);
            for (OsmNode node : nodeList) {
                mapper.map(ps, node);
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OsmNode get(long id) {
        String sql = "SELECT * FROM NODES WHERE ID = ?";

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            ps.setLong(1, id);
            OsmNode node = null;
            if (rs.next()) {
                node = (OsmNode) mapper.mapRow(rs, 1);
            }
            return node;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<OsmNode> get() {
        return null;
    }

    @Override
    public void delete() {
        String sql = "TRUNCATE NODES";
        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, Integer> getUserCount() {
        String sql = "SELECT USERNAME, COUNT(USERNAME) AS CNT FROM NODES GROUP BY USERNAME";
        Map<String, Integer> userCount = new TreeMap<>();
        try (Connection conn = dataSource.getConnection();
             Statement ps = conn.createStatement();
             ResultSet rs = ps.executeQuery(sql)) {
            while (rs.next()) {
                userCount.put(rs.getString("USERNAME"), rs.getInt("CNT"));
            }
        } catch (SQLException ex) {
            logger.info("Cannot select unique users: " + ex.getMessage());
        }
        return userCount;
    }

    public void setIndex() {
        String sql = "CREATE INDEX meters_index ON NODES USING gist (ll_to_earth(lat, lon))";
        try (Connection conn = dataSource.getConnection();
        Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException ex) {
            logger.error("Cannot create index: " + ex.getMessage());
        }
    }

}
