package com.dis.task1.dao.nodes;

import com.dis.task1.utils.xml.schema.generatedClasses.OsmNode;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NodesDao {

    void addStmt(OsmNode node);
    void addPrepStmt(OsmNode node);
    void addBatch(List<OsmNode> nodeList);
    OsmNode get(long id);
    List<OsmNode> get();
    void delete();
    void setIndex();

}
