package com.dis.task1.dao.mapper;


import com.dis.task1.utils.xml.schema.generatedClasses.OsmRelation;
import com.dis.task1.utils.xml.schema.generatedClasses.OsmWay;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WayMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        return null;
    }

    public void map(PreparedStatement ps, Object obj) throws SQLException{
        if (obj instanceof OsmWay) {
            ps.setLong(1, ((OsmWay) obj).getId());
            ps.setString(2, ((OsmWay) obj).getUser());
            ps.setDate(3, new java.sql.Date(((OsmWay) obj).getTimestamp().toGregorianCalendar().getTime().getTime()));
            //ps.setBoolean(4, ((OsmWay) obj).isVisible());
        } else if (obj instanceof OsmRelation) {
            ps.setLong(1, ((OsmRelation) obj).getId());
            ps.setString(2, ((OsmRelation) obj).getUser());
            ps.setDate(3, new Date(((OsmRelation) obj).getTimestamp().toGregorianCalendar().getTime().getTime()));

        }

    }
}
