package com.dis.task1.dao.ways;


import com.dis.task1.utils.xml.schema.generatedClasses.OsmWay;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface WayDao {
    void addBatch(List<OsmWay> wayList);
}
