package com.dis.task1;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Properties;

@SpringBootApplication
public class Application{

    private final static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String... args) {

        Options options = new Options();
        options.addOption("n", "name", true, "filename after decompress");
        options.addOption("f", "file", true, "input file");

        logger.info("Parse input args.");
        CommandLineParser parser = new DefaultParser();
        Properties properties = System.getProperties();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption('n')) {
                String decompressedFile = cmd.getOptionValue('n');
                properties.setProperty("decFilepath", decompressedFile);
                logger.info("parsing decompressed file path: " + decompressedFile);
            }
            if (cmd.hasOption('f')) {
                String filepath = cmd.getOptionValue('f');
                properties.setProperty("filepath", filepath);
                logger.info("parsing archive path: " + filepath);
            }
        } catch (ParseException ex) {
            logger.error("Cannot parse parameters. " + ex.getMessage());
        }

        SpringApplication.run(Application.class, args);
        logger.info("Spring Boot Application.run()");
    }
}
