package com.dis.task1.repository;

import com.dis.task1.model.Nodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface NodesRepository extends JpaRepository<Nodes, Long> {

    List<Nodes> getNodesByUid(long uid);

    Nodes getById(long id);

    @Query("SELECT username, count(username) as cnt " +
            "from Nodes group by username order by cnt DESC")
    //Map<String, Integer> getNodesMap();
    List<Object> getNodesMap();

    @Query(value = "SELECT * FROM nodes\n" +
            "WHERE earth_box(ll_to_earth(?1, ?2), ?3) @> " +
            "ll_to_earth(lat, lon);\n", nativeQuery = true)
    List<Nodes> getNodesInRange(double lat, double lon, double range);

}
