package com.dis.task1.repository;

import com.dis.task1.model.Way;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WaysRepository extends JpaRepository<Way, Long>{
}
