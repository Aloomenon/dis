package com.dis.task1.repository;

import com.dis.task1.model.Relation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RelationsRepository extends JpaRepository<Relation, Long> {
}
